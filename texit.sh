#!/bin/bash
# TeXit invokes your TeX build scripts.
#
# Usage:
# If no arguments are given, the script searches recursively for *.tex-files
# starting with the current working directory as root and invokes *.tpbuild-
# scripts if there are any.
# Optionally you can specify *.tex-files with or without extension, then only
# the passed files are processed. This is especially useful when called from
# the folder where the tex-file resides.
#
# You should be able to call it from the root of your filesystem to compile all
# your tex-files you have on your system. I won't recommend doing so.
#
# Author: Maximilian Blochberger <blochberger.max@gmail.com>

workingDir=$(pwd)

# Define some really important things ;)
if [ -t 0 ]; then
	red=$(tput setaf 1)
	green=$(tput setaf 2)
	yellow=$(tput setaf 3)
	bold=$(tput bold)
	normal=$(tput sgr0)
fi

#=== Helpers ===================================================================

# Print OpenRC-like status message - you'll probably recognize it from the boot
# process of you linux distribution (fe. gentoo)
echoStatus() {
	if [ -t 0 ]; then
		# Query cols here - in case you change the terminal size while still running
		cols=$(tput cols)
		printf "\r"
		tput cuf $(($cols - (${#2} + 3)))
	fi
	echo " [$1$2$normal]"
}

#===============================================================================

# Start gathering information/details about files
if [ $# -eq 0 ]; then
	IFS=$'\n'; for file in $(find ${workingDir} -name "*.tex" 2>/dev/null); do
		files[${#files[*]}]="$file"
	done; unset IFS
else
	for arg in "$@"; do
		if [ ! -z "${arg}" ]; then
			# Append .tex if needed
			if [[ ! "${arg}" =~ ^.*\.tex$ ]]; then
				arg="${arg}.tex"
			fi
			# Prepend absolute path if needed
			if [[ ! "$arg" =~ ^/.*$ ]]; then
				# No need to differ between relative paths and filenames
				arg="${workingDir}/$arg"
			fi
			files[${#files[*]}]="$arg"
		fi
	done
fi

# TODO Normalize file paths (fe. /tmp/../ -> /)

# Run build-scripts if existing
for file in "${files[@]}"; do
	fileNoExt="${file%.*}"
	name="${fileNoExt##*/}"
	buildFile="${fileNoExt}.tpbuild"
	buildLog="${fileNoExt}.tpbuild.log"
	echo -n "Building '${name}'..."
	if [ ! -f "$file" ]; then
		echoStatus $yellow "skip"
		echo "Source file does not exist: ${file}"
	elif [ ! -f "$buildFile" ]; then
		echoStatus $yellow "skip"
		echo "Build file does not exist: ${buildFile}"
	else
		export TEXPAD_ROOTFILE="$file"
		export TEXPAD_ROOTFILE_NO_EXT="$fileNoExt"
		"$buildFile" >& "$buildLog"
		if [ $? -eq 0 ]; then
			echoStatus $green " ok "
			rm -f "${buildLog}"
		else
			echoStatus $red "fail"
			echo "See '${buildLog}' for details."
		fi
		unset TEXPAD_ROOTFILE
		unset TEXPAD_ROOTFILE_NO_EXT
	fi
done
